export const PLUGIN_ID = "jupyterlab-hide-cells";
export const METADATA_KEY_HIDDEN_CELL_ENABLED = PLUGIN_ID + ":hidden";
export const HIDDENCELL_ENABLED_CLASS = "hidden-enabled";
export const TEST_CELL_METADATA_KEY = PLUGIN_ID + ":isTestCell";
export const TEST_SUCCESSFUL_METADATA_KEY = PLUGIN_ID + ":lastTestSuccessful";
